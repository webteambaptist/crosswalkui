﻿var _searchID;
var table;
$(document).ready(function () {
    $("#DeptId").attr("disabled", "disabled");
    $('#example').hide();
    // Generate DataTable
//    table = $('#example').DataTable({
//        "searching": false,
//        "bAutoWidth": true,
//        "ajax": {
//            "url": "Home/GetAllData",
//            "error": function (errResp) {
//                alert("There was an issue processing your request. Please try again.");
//            },
//            "dataSrc": function (json) {
//                var data = JSON.parse(json.data);
//                return data;
//            },
//            "type": "GET",
//            "datatype": "json"
//        },
//        "language": {
//            "emptyTable": "No records available"
//        },
//        "aoColumns": [

//            { mData: 'OldOrg', sDefaultContent: '' },
//            { mData: 'OldDept', sDefaultContent: '' },
//            { mData: 'OldAcct', sDefaultContent: '' },
//            { mData: 'OldAccountCode', sDefaultContent: '' },
//            { mData: 'OldDesc', sDefaultContent: '' },
//            { mData: 'NewOrg', sDefaultContent: '' },
//            { mData: 'NewDept', sDefaultContent: '' },
//            { mData: 'NewAcct', sDefaultContent: '' },
//            { mData: 'NewAccountCode', sDefaultContent: '' },
//            { mData: 'NewDesc', sDefaultContent: '' },
//            { mData: 'OldOrgDesc', sDefaultContent: '' },
//            { mData: 'OldDeptDesc', sDefaultContent: '' },
//            { mData: 'NewOrgDesc', sDefaultContent: '' },
//            { mData: 'DeptDesc', sDefaultContent: '' },
//        ],
//        "pageLength": 10,
//        //If they want the data to be displayed when the page initially loads.
//        "pagingType": "full_numbers",
//        responsive: true
//    }).columns([10,11,12,13]).visible(false, false);

//    ////If they dont want anything to display when the page initiailly loads, hide the headers and the paging.
//    //    "pagingType": "none",
//    //    responsive: true
//    //}).columns([0,1,2,3,4,5,6,7,8,9]).visible(false, false);  
});

$("#Search").click(function () {
    $('#example').show();
    var orgid = $("#OrgId").val();
    var acctid = $("#AcctId").val();
    var deptid = $("#DeptId").val();

    if (orgid == "") {
        orgid = null;
    }
    if (acctid == "") {
        acctid = null;
    }
    if (deptid == "") {
        deptid = null;
    }

    //Set SearchOption Value to determine which table to display
    if (orgid != null && acctid == null && deptid == null) {
        _searchID = 1;
    }
    else if (orgid == null && acctid != null && deptid == null) {
        _searchID = 2;
    }
    else if (orgid != null && acctid == null && deptid != null) {
        _searchID = 3;
    }
    else if (orgid != null && acctid != null && deptid != null) {
        _searchID = 4;
    }
    else if (orgid != null && acctid != null && deptid == null) {
        _searchID = 5;
    }

    //Search by Org/Company
    if (_searchID == 1) {
        //Get Values
        table = $('#example').DataTable({
            "destroy": true,
            "searching": false,
            "bAutoWidth": true,
            "ajax": {
                "url": "/Home/GetSearchData",
                "error": function (errResp) {
                    //alert("There was an issue processing your request. Please try again.");
                    alert(errResp.responseText);
                },
                "data": {
                    SearchOption: _searchID,
                    OldOrg: orgid
                },
                "dataSrc": function (json) {
                    var data = JSON.parse(json.data);
                    return data;
                },
                "type": "GET",
                "datatype": "json"
            },
            "language": {
                "emptyTable": "No records available based on your search by Old Company (Org) ID. Please modify search and try again."
            },
            "aoColumns": [
                {
                    "width": "15%",
                    mData: 'OldOrg', sDefaultContent: ''
                },
                { mData: 'OldOrgDesc', sDefaultContent: '' },
                { mData: 'OldDept', sDefaultContent: '' },
                { mData: 'OldDeptDesc', sDefaultContent: '' },
                { mData: 'OldAcct', sDefaultContent: '' },
                { mData: 'OldDesc', sDefaultContent: '' },
                { mData: 'OldAccountCode', sDefaultContent: '' },
                
                {
                    "width": "15%",
                    mData: 'NewOrg', sDefaultContent: ''
                },
                { mData: 'NewOrgDesc', sDefaultContent: '' },
                { mData: 'NewDept', sDefaultContent: '' },
                { mData: 'DeptDesc', sDefaultContent: '' },
                { mData: 'NewAcct', sDefaultContent: '' },
                { mData: 'NewDesc', sDefaultContent: '' },
                { mData: 'NewAccountCode', sDefaultContent: '' },       
            ],
            "pageLength": 10,
            "pagingType": "full_numbers",
            "paging": false,
            "info": false,
            responsive: true
        }).columns([2,3,4,5,6,9,10,11,12,13]).visible(false, false);
    }
    //Search by Acct ID 
    else if (_searchID == 2) {
        //Generate Datatable
        table = $('#example').DataTable({
            "destroy": true,
            "searching": false,
            "bAutoWidth": true,
            "ajax": {
                "url": "/Home/GetSearchData",
                "error": function (errResp) {
                    alert("There was an issue processing your request. Please try again.");
                },
                "data": {
                    SearchOption: _searchID,
                    OldAcct: acctid
                },
                "dataSrc": function (json) {
                    var data = JSON.parse(json.data);
                    return data;
                },
                "type": "GET",
                "datatype": "json"
            },
            "language": {
                "emptyTable": "No records available based on your search. Please modify search and try again."
            },
            "aoColumns": [
                { mData: 'OldOrg', sDefaultContent: ''},
                { mData: 'OldOrgDesc', sDefaultContent: '' },
                { mData: 'OldDept', sDefaultContent: '' },
                { mData: 'OldDeptDesc', sDefaultContent: '' },
                { "width": "10%", mData: 'OldAcct', sDefaultContent: '' },
                { mData: 'OldDesc', sDefaultContent: '' },
                { mData: 'OldAccountCode', sDefaultContent: '' },
                { mData: 'NewOrg', sDefaultContent: '' },
                { mData: 'NewOrgDesc', sDefaultContent: '' },
                { mData: 'NewDept', sDefaultContent: '' },
                { mData: 'DeptDesc', sDefaultContent: '' },
                { "width": "10%", mData: 'NewAcct', sDefaultContent: '' },
                { mData: 'NewDesc', sDefaultContent: '' },
                { mData: 'NewAccountCode', sDefaultContent: '' },   
            ],
            "pageLength": 10,
            "pagingType": "full_numbers",
            "paging": false,
            "info": false,
            responsive: true
        }).columns([0,1,2,3,6,7,8,9,10,13]).visible(false, false);
    }
    //Search by Dept ID && Org ID
    else if (_searchID == 3) {
        if (orgid == null || orgid == "") {
            alert("To search based on Dept and Org, Org ID field cannot be empty. Please enter an Org Id and try your search again.");
        }
        else {
            //Generate DataTable
            table = $('#example').DataTable({
                "destroy": true,
                "searching": false,
                "bAutoWidth": true,
                "ajax": {
                    "url": "/Home/GetSearchData",
                    "error": function (errResp) {
                        alert("There was an issue processing your request. Please try again.");
                    },
                    "data": {
                        SearchOption: _searchID,
                        OldOrg: orgid,
                        OldDept: deptid
                    },
                    "dataSrc": function (json) {
                        var data = JSON.parse(json.data);
                        return data;
                    },
                    "type": "GET",
                    "datatype": "json"
                },
                "language": {
                    "emptyTable": "No records available based on your search by Dept ID & Old Company (Org) ID. Please modify search and try again."
                },
                "aoColumns": [
                    //{ mData: 'OldOrg', sDefaultContent: '' },
                    //{
                    //    "width": "10%",
                    //    mData: 'OldDept', sDefaultContent: ''
                    //},
                    //{ mData: 'OldAcct', sDefaultContent: '' },
                    //{ mData: 'OldAccountCode', sDefaultContent: '' },
                    //{ mData: 'OldDesc', sDefaultContent: '' },
                    //{ mData: 'NewOrg', sDefaultContent: '' },
                    //{
                    //    "width": "11%",
                    //    mData: 'NewDept', sDefaultContent: ''
                    //},
                    //{ mData: 'NewAcct', sDefaultContent: '' },
                    //{ mData: 'NewAccountCode', sDefaultContent: '' },
                    //{ mData: 'NewDesc', sDefaultContent: '' },
                    //{ mData: 'OldOrgDesc', sDefaultContent: '' },
                    //{ mData: 'OldDeptDesc', sDefaultContent: '' },
                    //{ mData: 'NewOrgDesc', sDefaultContent: '' },
                    //{ mData: 'DeptDesc', sDefaultContent: '' },
                    { mData: 'OldOrg', sDefaultContent: '' },
                    { mData: 'OldOrgDesc', sDefaultContent: '' },
                    { "width": "10%", mData: 'OldDept', sDefaultContent: '' },
                    { mData: 'OldDeptDesc', sDefaultContent: '' },
                    { mData: 'OldAcct', sDefaultContent: '' },
                    { mData: 'OldDesc', sDefaultContent: '' },
                    { mData: 'OldAccountCode', sDefaultContent: '' },
                    { mData: 'NewOrg', sDefaultContent: '' },
                    { mData: 'NewOrgDesc', sDefaultContent: '' },
                    { "width": "11%", mData: 'NewDept', sDefaultContent: '' },
                    { mData: 'DeptDesc', sDefaultContent: '' },
                    { mData: 'NewAcct', sDefaultContent: '' },
                    { mData: 'NewDesc', sDefaultContent: '' },
                    { mData: 'NewAccountCode', sDefaultContent: '' },   
                ],
                "pageLength": 10,
                "pagingType": "full_numbers",
                "paging": false,
                "info": false,
                responsive: true
            }).columns([4,5,6,11,12,13]).visible(false, false);
        }
    }
    //Search by Org ID, AcctID & DeptID
    else if (_searchID == 4) {
        //Generate Datatable
        table = $('#example').DataTable({
            "destroy": true,
            "searching": false,
            "bAutoWidth": true,
            "ajax": {
                "url": "/Home/GetSearchData",
                "error": function (errResp) {
                    alert("There was an issue processing your request. Please try again.");
                },
                "data": {
                    SearchOption: _searchID,
                    OldOrg: orgid,
                    OldAcct: acctid,
                    OldDept: deptid
                },
                "dataSrc": function (json) {
                    var data = JSON.parse(json.data);
                    return data;
                },
                "type": "GET",
                "datatype": "json"
            },
            "language": {
                "emptyTable": "No records available based on your search. Please modify search and try again."
            },
            "aoColumns": [
                //{ mData: 'OldOrg', sDefaultContent: '' },
                //{ mData: 'OldDept', sDefaultContent: '' },
                //{ mData: 'OldAcct', sDefaultContent: '' },
                //{
                //    "width": "25%",
                //    mData: 'OldAccountCode', sDefaultContent: ''
                //},
                //{ mData: 'OldDesc', sDefaultContent: '' },
                //{ mData: 'NewOrg', sDefaultContent: '' },
                //{ mData: 'NewDept', sDefaultContent: '' },
                //{ mData: 'NewAcct', sDefaultContent: '' },
                //{
                //    "width": "32%",
                //    mData: 'NewAccountCode', sDefaultContent: ''
                //},
                //{
                //    "width": "40%",
                //    mData: 'NewDesc', sDefaultContent: ''
                //},
                //{ mData: 'OldOrgDesc', sDefaultContent: '' },
                //{ mData: 'OldDeptDesc', sDefaultContent: '' },
                //{ mData: 'NewOrgDesc', sDefaultContent: '' },
                //{ mData: 'DeptDesc', sDefaultContent: '' },
                { mData: 'OldOrg', sDefaultContent: '' },
                { mData: 'OldOrgDesc', sDefaultContent: '' },
                { mData: 'OldDept', sDefaultContent: '' },
                { mData: 'OldDeptDesc', sDefaultContent: '' },
                { mData: 'OldAcct', sDefaultContent: '' },
                { mData: 'OldDesc', sDefaultContent: '' },
                { "width": "25%", mData: 'OldAccountCode', sDefaultContent: '' },
                { mData: 'NewOrg', sDefaultContent: '' },
                { mData: 'NewOrgDesc', sDefaultContent: '' },
                { mData: 'NewDept', sDefaultContent: '' },
                { mData: 'DeptDesc', sDefaultContent: '' },
                { mData: 'NewAcct', sDefaultContent: '' },
                { "width": "40%", mData: 'NewDesc', sDefaultContent: '' },
                { "width": "32%", mData: 'NewAccountCode', sDefaultContent: '' },   
            ],
            "pageLength": 10,
            "pagingType": "full_numbers",
            "paging": false,
            "info": false,
            responsive: true
        });
    }
    // Acct ID && Org ID
    else if (_searchID == 5) {
        table = $('#example').DataTable({
            "destroy": true,
            "searching": false,
            "bAutoWidth": true,
            "ajax": {
                "url": "/Home/GetSearchData",
                "error": function (errResp) {
                    alert("There was an issue processing your request. Please try again.");
                },
                "data": {
                    SearchOption: _searchID,
                    OldOrg: orgid,
                    OldAcct: acctid,
                },
                "dataSrc": function (json) {
                    var data = JSON.parse(json.data);
                    return data;
                },
                "type": "GET",
                "datatype": "json"
            },
            "language": {
                "emptyTable": "No records available based on your search. Please modify search and try again."
            },
            "aoColumns": [
                { mData: 'OldOrg', sDefaultContent: '' },
                { mData: 'OldOrgDesc', sDefaultContent: '' },
                { mData: 'OldDept', sDefaultContent: '' },
                { mData: 'OldDeptDesc', sDefaultContent: '' },
                { mData: 'OldAcct', sDefaultContent: '' },
                { mData: 'OldDesc', sDefaultContent: '' },
                { mData: 'OldAccountCode', sDefaultContent: '' },
                { mData: 'NewOrg', sDefaultContent: '' },
                { mData: 'NewOrgDesc', sDefaultContent: '' },
                { mData: 'NewDept', sDefaultContent: '' },
                { mData: 'DeptDesc', sDefaultContent: '' },
                { mData: 'NewAcct', sDefaultContent: '' },
                { mData: 'NewDesc', sDefaultContent: '' },
                { mData: 'NewAccountCode', sDefaultContent: '' },
            ],
            "pageLength": 10,
            "pagingType": "full_numbers",
            "paging": false,
            "info": false,
            responsive: true
        }).columns([2,3,6,9,10,13]).visible(false, false);

    }

});
$("#Reset").click(function () {
    window.location.href = "../";
});
function EnableDept() {
    if ($("#OrgId").val() == "" || $("#OrgId").val() == null) {
        $("#DeptId").attr("disabled", "disabled");
    }
    else {
        $("#DeptId").removeAttr("disabled"); 
    }
}
