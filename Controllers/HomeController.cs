﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GLXWALK.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace GLXWALK.Controllers
{
    public class HomeController : Controller
    {
        private readonly GLXWALK_BContext _context;
        public HomeController (GLXWALK_BContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetAllData()
        {
            var jsonData = "";
            var initialData = GETDATA();

            jsonData = JsonConvert.SerializeObject(initialData.Value);
            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        private JsonResult GETDATA()
        {
            var alldata = _context.GlxwalkB.Take(1000).ToList();
     
            return Json(alldata);
        }

        public ActionResult GetSearchData(int SearchOption, string OldOrg, string OldAcct, string OldDept)
        {
            var _OrgID = "";
            var _AcctID = "";
            var _DeptID = "";
            var jsonData = "";

            //Set values passed from js
            if (OldOrg != null)
            {
                _OrgID = OldOrg;
            }
            if (OldAcct != null)
            {
                _AcctID = OldAcct;
            }
            if (OldDept != null)
            {
                _DeptID = OldDept;
            }
            
            //Search by Org ID
            if (SearchOption == 1)
            {
                var _searchbyOrgID = _context.GlxwalkB.Where(x => x.OldOrg == _OrgID).Take(1).ToList();
                jsonData = JsonConvert.SerializeObject(_searchbyOrgID);
            }
            //Search by Acct ID
            else if (SearchOption == 2)
            {
                if (OldOrg == null)
                {
                    var _searchbyAcctID = _context.GlxwalkB.Where(x => x.OldAcct == _AcctID).Take(1).ToList();
                    jsonData = JsonConvert.SerializeObject(_searchbyAcctID);
                }
            }
            //Search by Dept ID && Org ID
            else if (SearchOption == 3)
            {
                var _searchbyDeptandOrgID = _context.GlxwalkB.Where(x => x.OldDept == _DeptID && x.OldOrg == _OrgID).Take(1).ToList();
                //var _searchbyDeptandOrgID = _context.GlxwalkB.Where(x => x.OldDept == _DeptID && x.OldOrg == _OrgID).ToList();
                jsonData = JsonConvert.SerializeObject(_searchbyDeptandOrgID);
            }
            //Search by Org ID, Acct ID & Dept ID
            else if (SearchOption == 4)
            {
                var _searchbyDeptOrgandAcct = _context.GlxwalkB.Where(x => x.OldOrg == _OrgID && x.OldAcct == _AcctID && x.OldDept == _DeptID).Take(1).ToList();
                jsonData = JsonConvert.SerializeObject(_searchbyDeptOrgandAcct);
            }
            //Search by Acct ID && Org ID
            else if (SearchOption == 5)
            {
                var _searchbyOrgandAcctID = _context.GlxwalkB.Where(x => x.OldAcct == _AcctID && x.OldOrg == _OrgID).Take(1).ToList();
                jsonData = JsonConvert.SerializeObject(_searchbyOrgandAcctID);
            }
            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }
    }
}
