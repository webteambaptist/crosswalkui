﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GLXWALK.Models
{
    public partial class GLXWALK_BContext : DbContext
    {
        public GLXWALK_BContext()
        {
        }

        public GLXWALK_BContext(DbContextOptions<GLXWALK_BContext> options)
            : base(options)
        {
        }

        public virtual DbSet<GlxwalkB> GlxwalkB { get; set; }
        public virtual DbSet<GlxwalkBArchive> GlxwalkBArchive { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=BHTMSQSTC01V;Database=GLXWALK_B;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<GlxwalkB>(entity =>
            {
                entity.ToTable("_GLXWALK_B");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DeptDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NewAccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewAcct)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewAcctType)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NewDept)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NewOrg)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewOrgDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldAccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldAcct)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldDept)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldDeptDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldOrg)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldOrgDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GlxwalkBArchive>(entity =>
            {
                entity.ToTable("_GLXWALK_B_Archive");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DeptDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NewAccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewAcct)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewAcctType)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NewDept)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NewOrg)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewOrgDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldAccountCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldAcct)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldDept)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldDeptDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OldOrg)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldOrgDesc)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });
        }
    }
}
