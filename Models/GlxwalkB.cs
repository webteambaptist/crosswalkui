﻿using System;
using System.Collections.Generic;

namespace GLXWALK.Models
{
    public partial class GlxwalkB
    {
        public int Id { get; set; }
        public string OldOrg { get; set; }
        public string OldDept { get; set; }
        public string OldAcct { get; set; }
        public string OldAccountCode { get; set; }
        public string OldDesc { get; set; }
        public string NewOrg { get; set; }
        public string NewDept { get; set; }
        public string NewAcct { get; set; }
        public string NewAccountCode { get; set; }
        public string NewDesc { get; set; }
        public string NewAcctType { get; set; }
        public string OldOrgDesc { get; set; }
        public string OldDeptDesc { get; set; }
        public string NewOrgDesc { get; set; }
        public string DeptDesc { get; set; }
    }
}
